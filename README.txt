
--------------------------------------------------------------------------------
                              Drupal Commerce Goodie Bag
--------------------------------------------------------------------------------

Maintainers: 
 * Praveen Karan (mudi..), praveenkaran.engg@gmail.com


This module is used to display drupal commerce goodie bag or goods bag block.
In this block you can add multiple products then update your bag quantity.

Goodie bag block containing following data :-

Product name with price
Total bag quantity
Price per bag
Total order

Features
------------

1. You can add multiple products in single bag.

2. You can add multiple quantity of bag.

Recommended Module
--------------------

* Drupal Commerce(https://www.drupal.org/project/commerce).
