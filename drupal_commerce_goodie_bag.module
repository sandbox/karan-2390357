<?php
/**
 * @file
 * The purpose of this module is to add multiple products in single bag.
 */

/**
 * Implements hook_help().
 */
function drupal_commerce_goodie_bag_help($path, $arg) {
  switch ($path) {
    case 'admin/help#drupal_commerce_goodie_bag':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t("User can add multiple products in single bag and then he can able add multiple quantity of that bag.") . '<p>';
      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function drupal_commerce_goodie_bag_permission() {
  return array(
    'administer goodie bag' => array(
      'title' => t('Administer goodie bag'),
      'description' => t('Perform administration tasks for goodie bag configuration page.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function drupal_commerce_goodie_bag_menu() {
  $items = array();
  $items['cart/empty'] = array(
    'title' => 'Empty Cart',
    'access arguments' => array('access content'),
    'page callback' => 'drupal_commerce_goodie_bag_empty_cart',
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/people/goodie_bag_config'] = array(
    'title' => 'Goodie bag configuration',
    'description' => 'Goodie bag configuration page',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('drupal_commerce_goodie_bag_config_form'),
    'access arguments' => array('administer goodie bag'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * The order with the product line items all removed.
 */
function drupal_commerce_goodie_bag_empty_cart() {
  global $user;
  // Load the order and empty the cart.
  $order = commerce_cart_order_load($user->uid);
  commerce_cart_order_empty($order);

  // As this page won't display anything you need to redirect somewhere.
  drupal_set_message(t('Cart has been empty.'));
  drupal_goto('node');
}

/**
 * Implements hook_page_build().
 */
function drupal_commerce_goodie_bag_page_build() {
  drupal_add_js(drupal_get_path('module', 'drupal_commerce_goodie_bag') . '/goodie_bag.js', array('scope' => 'footer'));
}

/**
 * Implements hook_block_info().
 */
function drupal_commerce_goodie_bag_block_info() {
  $blocks = array();
  $blocks['drupal_commerce_goodie_bag_block'] = array(
    'info' => t('Drupal Goodie Bag'),
    'cache' => DRUPAL_NO_CACHE);
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function drupal_commerce_goodie_bag_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'drupal_commerce_goodie_bag_block':
      $block['subject'] = 'Goodie Bag';
      $block['content'] = drupal_render(drupal_get_form('drupal_commerce_goodie_bag_form'));
      break;
  }
  return $block;
}

/**
 * Implements hook_form().
 */
function drupal_commerce_goodie_bag_form($form, &$form_state) {
  $form['goodie_bag_qty'] = array(
    '#type' => 'textfield',
    '#id' => 'numbertype',
    '#default_value' => variable_get('drupal_commerce_goodie_bag_qty_default'),
    '#size' => 20,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['Submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add to Bag'),
    '#weight' => 4,
  );

  // Get the path to the module.
  $path = drupal_get_path('module', 'drupal_commerce_goodie_bag');

  // Attach the CSS to the form.
  $form['#attached'] = array(
    'css' => array(
      'type' => 'file',
      'data' => $path . '/drupal_commerce_goodie_bag.css',
    ),
  );
  return $form;
}

/**
 * Implements hook_theme().
 */
function drupal_commerce_goodie_bag_theme() {
  return array(
    'drupal_commerce_goodie_bag_form' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Creating theme form.
 */
function theme_drupal_commerce_goodie_bag_form($variables) {
  global $user;
  global $base_url;

  // Isolate the form definition form the $variables array.
  $form = $variables['form'];
  $output = '';

  // Put the entire structure into a div that can be used for
  // CSS purposes
  // Each of the pieces of text is wrapped in a <span>
  // tag to allow it to be floated left
  // Prepare the display of the default Shopping Cart block.
  // Default to an empty cart block message.
  // First check to ensure there are products in the shopping cart.
  if ($order = commerce_cart_order_load($user->uid)) {
    $cur = commerce_currency_load();
    $sym = $cur['symbol'];

    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    // If there are one or more products in the cart...
    if (commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types()) > 0) {
      $view = views_get_view('commerce_cart_block');
      $view->set_display('page');
      $view->pre_execute();
      $view->set_arguments(array($order->order_id));
      $view->execute('default');
      $output .= '<table>
          <thead>
            <tr style="border:1px solid black">
              <td style="font-weight:bold">Product Name</td>
              <td style="font-weight:bold">Price</td>
            </tr>
          </thead>
          <tbody>';
      for ($i = 0; $i < count($view->result); $i++) {
        $qty = $view->result[$i]->commerce_line_item_field_data_commerce_line_items_quantity;
        $price = $view->result[$i]->_field_data['commerce_line_item_field_data_commerce_line_items_line_item_']['entity']->commerce_unit_price[LANGUAGE_NONE][0]['amount'];
        $product_id = $view->result[$i]->_field_data['commerce_line_item_field_data_commerce_line_items_line_item_']['entity']->commerce_product[LANGUAGE_NONE][0]['product_id'];
        $cart_arr[] = array($product_id);
        foreach (commerce_info_fields('commerce_product_reference') as $field) {
          // Build query.
          $query = new EntityFieldQuery();
          $query->entityCondition('entity_type', 'node', '=')
            ->fieldCondition($field['field_name'], 'product_id', $product_id, '=');
          $pr_detail = $query->execute();
        }

        $nodes = entity_load('node', array_keys($pr_detail['node']));
        $array_key = array_keys($pr_detail['node']);
        $result = $nodes[$array_key[0]]->title;
        $calcutated_pr = $qty * $price;
        $calcutated_pr = round($calcutated_pr);
        $final_pr = substr($calcutated_pr, 0, -2);
        $output .= '<tr>
          <td id="product_name">' . $result . '</td>
          <td id="product_price">' . $sym . $final_pr . '</td>
        </tr>';
      }
      $output .= '</tbody></table>';
      $total_order = 0;
      variable_set('drupal_commerce_goodie_bag_product_nid', $cart_arr);
      $price_per_bag = $order->commerce_order_total[LANGUAGE_NONE][0]['amount'];
      $price_per_bag = round($price_per_bag);
      $final_price_bag = substr($price_per_bag, 0, -2);
      $output .= '<span>' . t('Quantity') . '</span>';
      // Form elements are rendered with drupal_render()
      $output .= drupal_render($form['goodie_bag_qty']);
      $output .= '<div id="price_per_bag_onpagereload"><span>Price per bag : ' . $sym . $final_price_bag . '</span></div>
        <div id="goodie_bag_total_order"><span>Total Order : ' . $sym . '</span> <div id="total_order">' . $total_order . '</div></div>
        <div id="price_per_bag" style="display:none">' . $final_price_bag . '</div>';
      $output .= drupal_render_children($form);
      $output .= '<a href="' . $base_url . '/cart/empty">Clear Bag</a>';
      if (in_array('administrator', $user->roles) &&  variable_get('drupal_commerce_goodie_bag_qty_default') == '') {
        $output .= '<a href="' . $base_url . '/goodie_bag/config">Please add Goodie bag default quantity value </a>';
      }
    }
    else {
      $output = theme('commerce_cart_empty_block');
    }
  }
  // Pass the remaining form elements through drupal_render_children()
  // return the output.
  return $output;
}

/**
 * Form submit function.
 */
function drupal_commerce_goodie_bag_form_submit($form, &$form_state) {
  $product_nids = variable_get('drupal_commerce_goodie_bag_product_nid');
  global $user;
  $order = commerce_cart_order_load($user->uid);
  commerce_cart_order_empty($order);

  for ($i = 0; $i < count($product_nids); $i++) {
    $pid = $product_nids[$i][0];
    $quantity = $form_state['values']['goodie_bag_qty'];

    if ($product = commerce_product_load($pid)) {
      $uid = $user->uid;
      $line_item = commerce_product_line_item_new($product, $quantity);
      $line_item = commerce_cart_product_add($uid, $line_item, FALSE);
    }
  }
  drupal_goto('cart');
}

/**
 * Implements hook_form().
 */
function drupal_commerce_goodie_bag_config_form($form, &$form_state) {
  $qty_value = '';
  $goodie_bag_default_qty = variable_get('drupal_commerce_goodie_bag_qty_default');
  if (isset($goodie_bag_default_qty)) {
    $qty_value = $goodie_bag_default_qty;
  }

  $form['goodie_bag_default_qty'] = array(
    '#title' => 'Goodie bag default quantity value',
    '#type' => 'textfield',
    '#default_value' => $qty_value,
    '#size' => 20,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['Submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 4,
  );

  return $form;
}

/**
 * Form submit function.
 */
function drupal_commerce_goodie_bag_config_form_submit($form, &$form_state) {
  $qty_default = $form_state['values']['goodie_bag_default_qty'];
  variable_set('drupal_commerce_goodie_bag_qty_default', $qty_default);
}
