/**
 * @file
 * Javascript related to the main view list.
 */

(function ($) {
  Drupal.behaviors.gb = {
    attach: function(context, settings) {

      $(document).ready(function() {
        var qty = $("#numbertype").val();
        var price_bag = $("#price_per_bag").html();
        var total = qty * price_bag;
        if(total > 0) {
          $('#total_order').html(total);
        }
      });

      $("#numbertype").change(function() {
        var qty = $("#numbertype").val();
        var price_bag = $("#price_per_bag").html();
        var total = qty * price_bag;
        if(total > 0) {
          $('#total_order').html(total);
          $('#pr_qty').html(qty);
        }
      });
    }
  };
})(jQuery);
